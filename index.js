import express from 'express';
import Promise from 'bluebird';
import Logger from 'debug';
import v1 from './app/index';
import Database from './models/index';
import { runtimeConfigs } from './configs/environment';
import Migrates from './migrations/index';


global.Promise = Promise;


const DBCon = Database();
const debug = Logger('APP:');

const app = express();

DBCon.then((DB) => {
  v1.locals.dataBase = DB;

  Migrates(DB.getModels());

  app.use('/v1', v1);

  app.listen(runtimeConfigs.startupPort, () => {
    debug(`App up and running in http://127.0.0.1:${runtimeConfigs.startupPort}`);
  });
});
