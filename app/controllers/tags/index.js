import _ from 'lodash';
import logger from 'debug';
import { bearerToken } from '../../middlewares/auth';

const debug = logger('Tags:');


// INDEX Method
const index = (req, res, next) => {
  // Load Models
  const { Tags } = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.transform(
    _.assign({ status: 'active' },
    _.pick(req.query, 'taxonomy', 'name', 'status')),
    (result, value, key) => {
      debug(result, value, key);
      const q = {};
      q[key] = new RegExp(value, 'i');
      return _.assign(result, q);
    },
  {});

  debug(query);

  // Count Data
  return Tags.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Tags.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// CREATE Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'taxonomy', 'thumbnail', 'infos', 'status');
  const { _id: siteId } = req.site;
  values.site = siteId;
  // Create User
  req.tag = new Models.Users(values);
  // Save User
  req.tag.save()
  .then(() => res.json(req.tag))
  .catch(next);
};
const edit = (req, res, next) => {
  const values = _.pick(req.body, 'name', 'taxonomy', 'thumbnail', 'infos', 'status');
  const { _id: siteId } = req.site;
  values.site = siteId;
  // Mutate User
  _.assignIn(req.tag, values);
  // Save User
  return req.tag.save()
  .then(res.json)
  .catch(next);
};
const show = (req, res) => res.json(req.tag);
// REMOVE Method
const remove = (req, res, next) => {
  debug('Trying to remove');
  req.tag.remove().then(() => res.status(201).end()).catch(next);
};
export const parameters = (appInstance) => {
  appInstance.param('tag_id', (req, res, next, id) => {
    // Load Models
    const { Tags } = req.app.getModels();

    Tags.findOne({ _id: id }).then((tag) => {
      if (!tag) { return next(new Error('Tag not found')); }
      req.tag = tag;
      return next();
    }).catch(next);
  });
};
export const appRoutes = (appInstance) => {
  appInstance.get('/sites/:site_id/tags/:tag_id', bearerToken, show);
  appInstance.put('/sites/:site_id/tags/:tag_id', bearerToken, edit);
  appInstance.delete('/sites/:site_id/tags/:tag_id', bearerToken, remove);
  appInstance.post('/sites/:site_id/tags', bearerToken, create);
  appInstance.get('/sites/:site_id/tags', bearerToken, index);
};

export default appRoutes;
