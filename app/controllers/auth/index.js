// import _ from 'lodash';
import logger from 'debug';

const debug = logger('Auth:');

const auth = (req, res) => {
  const { Apps, Users, Tokens } = req.app.getModels();
  const { client_id, secret, user, pass } = req.body;
  req.auth = {};
  Apps.findOne({ client_id }).then((app) => {
    debug(app);
    if (!app) throw new Error('Unable to Locate APP');
    if (app.secret !== secret) return new Error('Unable to Locate APP');
    req.auth.app = app;
    return Users.findOne({ email: user });
  })
  .then((usr) => {
    if (!usr) throw new Error('Unable to locate User');
    req.auth.user = usr;
    debug(usr);
    return usr.comparePassword(pass);
  })
  .then((pas) => {
    if (!pas) throw new Error('Invalid Password');
    /* eslint-disable */
    const app_id = req.auth.app._id;
    const user_id = req.auth.user._id;
    /* eslint-enable */
    const Token = new Tokens({ user: user_id, app: app_id });
    req.auth.token = Token;
    return Token.save();
  })
  .then(token => res.json({
    authType: 'Bearer',
    token: token.token,
    validUntil: token.validUntil,
  }))
  .catch((err) => {
    res.status(403).json({
      message: err.message,
    });
  });
};

export const parameters = null;

export const appRoutes = (appInstance) => {
  appInstance.post('/auth/token', auth);
};

export default appRoutes;
