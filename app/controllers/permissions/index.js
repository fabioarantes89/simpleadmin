import _ from 'lodash';
import logger from 'debug';

import { bearerToken } from '../../middlewares/auth';

const debug = logger('PERMISSIONS:');


const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.reduce(
    _.assign(_.pick(req.query, 'module', 'action', 'name')),
    (result, value, key) => _.assign({}, result, _.setWith({}, key, new RegExp(value, 'i'))),
    {});

  // Count Data
  return Models.Permissions.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Permissions.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// Create Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'module', 'action');
  req.permission = new Models.Permissions(values);
  req.permission.save()
  .then(permission => res.json(permission))
  .catch(next);
};
const edit = (req, res, next) => {
  // Filter Input values
  const values = _.pick(req.body, 'module', 'action');
  // Mutate Permission
  _.assignIn(req.permission, values);
  // Save Permission
  req.permission.save()
  .then(permission => res.json(permission))
  .catch(next);
};

const show = (req, res) => res.json(req.permission);

// REMOVE Method
const remove = (req, res, next) => {
  debug('Trying to remove');
  req.permission.remove().then(() => res.status(201).end()).catch(next);
};

export const parameters = (appInstance) => {
  appInstance.param('permission_id', (req, res, next, id) => {
    // Load Models
    const Models = req.app.getModels();

    Models.Permissions.findOne({ _id: id }).then((obj) => {
      if (!obj) { return next(new Error('Permission not found')); }
      req.permission = obj;
      return next();
    }).catch(next);
  });
};

export const appRoutes = (appInstance) => {
  appInstance.get('/permissions/:permission_id', bearerToken, show);
  appInstance.put('/permissions/:permission_id', bearerToken, edit);
  appInstance.delete('/permissions/:permission_id', bearerToken, remove);
  appInstance.post('/permissions', bearerToken, create);
  appInstance.get('/permissions', bearerToken, index);
};

export default appRoutes;
