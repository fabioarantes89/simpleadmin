import _ from 'lodash';
import logger from 'debug';
import { bearerToken } from '../../middlewares/auth';

const debug = logger('USERS:');


// INDEX Method
const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.transform(
    _.assign({ status: 'active' },
    _.pick(req.query, 'email', 'name', 'status')),
    (result, value, key) => {
      debug(result, value, key);
      const q = {};
      q[key] = new RegExp(value, 'i');
      return _.assign(result, q);
    },
  {});

  debug(query);

  // Count Data
  return Models.Users.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Users.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// CREATE Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'password', 'email', 'status', 'group');
  // Find Group
  Models.Groups.findOne({ _id: values.group }).then((group) => {
    if (!group) return next(new Error('Group not found'));
    // Group Found
    req.group = group;
    // Create User
    req.user = new Models.Users(values);
    // Save User
    return req.user.save();
  })
  .then(() => res.json(req.user))
  .catch(next);
};
const edit = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  const values = _.pick(req.body, 'name', 'password', 'email', 'status', 'group');
  Models.Groups.findOne({ _id: values.group }).then((group) => {
    if (!group) return next(new Error('Group not found'));
    // Group Found
    req.group = group;
    // Mutate User
    _.assignIn(req.user, values);
    // Save User
    return req.user.save();
  })
  .then(res.json)
  .catch(next);
};
const show = (req, res) => res.json(req.user);

// REMOVE Method
const remove = (req, res, next) => {
  debug('Trying to remove');
  req.user.remove().then(() => res.status(201).end()).catch(next);
};

export const parameters = (appInstance) => {
  appInstance.param('user_id', (req, res, next, id) => {
    // Load Models
    const Models = req.app.getModels();

    Models.Users.findOne({ _id: id }).then((user) => {
      if (!user) { return next(new Error('User not found')); }
      req.user = user;
      return next();
    }).catch(next);
  });
};
export const appRoutes = (appInstance) => {
  appInstance.get('/users/:user_id', bearerToken, show);
  appInstance.put('/users/:user_id', bearerToken, edit);
  appInstance.delete('/users/:user_id', bearerToken, remove);
  appInstance.post('/users', bearerToken, create);
  appInstance.get('/users', bearerToken, index);
};

export default appRoutes;
