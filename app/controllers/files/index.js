import _ from 'lodash';
import logger from 'debug';
import path from 'path';
import fs from 'fs';
import multer from 'multer';
import moment from 'moment';
import Promise from 'bluebird';

import { bearerToken } from '../../middlewares/auth';


Promise.promisifyAll(fs);

const debug = logger('Files:');


const initDirectoryStructure = () => {
  const m = moment().format('YYYY/MM/DD');
  const baseDir = path.resolve(__dirname, '../../../uploads/');
  let oldPath = '';
  return Promise.mapSeries(m.split('/'), (fileName) => {
    oldPath = path.resolve(baseDir, oldPath, fileName);
    return new Promise((res, rej) => {
      fs.mkdirAsync(path.resolve(baseDir, oldPath)).then(res).catch((err) => {
        if (err.code === 'EEXIST') { return res(path.resolve(baseDir, oldPath)); }
        return rej(err);
      });
    });
  }).then(() => path.resolve(baseDir, m));
};


const storage = multer.diskStorage({
  destination(req, file, cb) {
    initDirectoryStructure().then(fPath => cb(null, fPath)).catch(err => cb(err, null));
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
  },
});
const uploader = multer({
  storage,
  putSingleFilesInArray: true,
});


const index = (req, res, next) => {
  // Load Models
  const { Files } = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.reduce(_.assign(_.pick(req.query, 'name')), (result, value, key) => _.assign({}, result, _.setWith({}, key, new RegExp(value, 'i'))), {});

  // Count Data
  return Files.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Files.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// Create Method
const create = (req, res, next) => {
  // Load Models
  const { Files } = req.app.getModels();

  let values = _.assignIn({}, _.pick(req.body, 'title', 'credits', 'description'));
  if (req.file) {
    values = _.assignIn(values, { fileType: 'FILE', filename: req.file.filename });
  } else {
    values = _.assignIn({ fileType: 'URL' }, _.pick(req.body, 'filename', 'title', 'credits', 'description'));
  }

  req.fileM = new Files(values);
  return req.fileM.save()
  .then(() => res.json(req.fileM))
  .catch(next);
};

// SHOW Method
const show = (req, res) => res.json(req.fileM);

// EDIT Method
const edit = (req, res, next) => {
  // Filter Input values
  const values = _.pick(req.body, 'name', 'permissions');

  // Mutate Group
  _.assignIn(req.file, values);

  return req.file.save()
  .then(file => res.json(file))
  .catch(next);
};
const remove = (req, res, next) => {
  debug('Trying to remove');
  req.fileM.remove().then(() => res.status(201).end()).catch(next);
};

export const parameters = (appInstance) => {
  appInstance.param('file_id', (req, res, next, id) => {
    // Load Models
    const { Files } = req.app.getModels();
    Files.findOne({ _id: id }).then((obj) => {
      if (!obj) { return next(new Error('File not found')); }
      req.fileM = obj;
      return next();
    }).catch(next);
  });
};

export const appRoutes = (appInstance) => {
  appInstance.get('/sites/:site_id/files/:file_id', bearerToken, show);
  appInstance.put('/sites/:site_id/files/:file_id', bearerToken, edit);
  appInstance.delete('/sites/:site_id/files/:file_id', bearerToken, remove);
  appInstance.post('/sites/:site_id/files', bearerToken, uploader.single('file'), create);
  appInstance.get('/sites/:site_id/files', bearerToken, index);
};

export default appRoutes;
