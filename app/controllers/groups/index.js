import _ from 'lodash';
import logger from 'debug';

import { bearerToken } from '../../middlewares/auth';

const debug = logger('GROUPS:');


const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.reduce(_.assign(_.pick(req.query, 'name')), (result, value, key) => _.assign({}, result, _.setWith({}, key, new RegExp(value, 'i'))), {});

  // Count Data
  return Models.Groups.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Groups.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// Create Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'permissions');

  if (values.permissions || !_.isArray(values.permissions)) {
    values.permissions = [values.permissions];
  }

  Models.Permissions.find({ _id: { $in: values.permissions } }).exec((permissions) => {
    values.permissions = _.reduce(permissions, (result, value) => {
      const { _id } = value;
      result.push(_id);
      return result;
    }, []);

    req.group = new Models.Groups(values);
    return req.group.save();
  })
  .then(() => res.json(req.group))
  .catch(next);
};

// SHOW Method
const show = (req, res) => res.json(req.group);

// EDIT Method
const edit = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'permissions');

  if (values.permissions || !_.isArray(values.permissions)) {
    values.permissions = [values.permissions];
  }

  Models.Permissions.find({ _id: { $in: values.permissions } }).exec((permissions) => {
    values.permissions = _.reduce(permissions, (result, value) => {
      const { _id } = value;
      result.push(_id);
      return result;
    }, []);

    // Mutate Group
    _.assignIn(req.group, values);

    return req.group.save();
  })
  .then(group => res.json(group))
  .catch(next);
};
const remove = (req, res, next) => {
  debug('Trying to remove');
  req.group.remove().then(() => res.status(201).end()).catch(next);
};

// PERMISSIONS Method
const permissions = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Search Permissions
  Models.Permissions.find({ __id: { $in: req.group.permissions } })
  .exec()
  .then(perm => res.json(perm)).catch(next);
};


export const parameters = (appInstance) => {
  appInstance.param('group_id', (req, res, next, id) => {
    // Load Models
    const Models = req.app.getModels();

    Models.Groups.findOne({ _id: id }).populate('permissions').then((obj) => {
      if (!obj) { return next(new Error('Group not found')); }
      req.group = obj;
      return next();
    }).catch(next);
  });
};

export const appRoutes = (appInstance) => {
  appInstance.get('/groups/:group_id/permissions', bearerToken, permissions);
  appInstance.get('/groups/:group_id', bearerToken, show);
  appInstance.put('/groups/:group_id', bearerToken, edit);
  appInstance.delete('/groups/:group_id', bearerToken, remove);
  appInstance.post('/groups', bearerToken, create);
  appInstance.get('/groups', bearerToken, index);
};

export default appRoutes;
