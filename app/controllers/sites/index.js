import _ from 'lodash';
import logger from 'debug';

import { bearerToken } from '../../middlewares/auth';

const debug = logger('Sites:');


// INDEX Method
const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.reduce(
    _.assign({ status: 'active' }, _.pick(req.query, 'name', 'url')),
    (result, value, key) => _.assign({}, result, _.setWith({}, key, new RegExp(value, 'i'))),
    {});

  debug(query);
  // Count Data
  return Models.Sites.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Sites.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// CREATE Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'title', 'parent', 'uri', 'content_id', 'status');
  // Find Group
  req.site = new Models.Sites(values);
    // Save Site
  return req.site.save()
  .then(site => res.json(site))
  .catch(next);
};
const edit = (req, res, next) => {
  const values = _.pick(req.body, 'name', 'title', 'parent', 'uri', 'content_id', 'status');

  // Mutate Site
  _.assignIn(req.user, values);
  // Save User
  return req.site.save()
  .then(res.json)
  .catch(next);
};
const show = (req, res) => res.json(req.site);

// REMOVE Method
const remove = (req, res, next) => {
  req.site.remove().then(() => res.status(201).end()).catch(next);
};

// Editorials
const editorials = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  const { _id } = req.site;

  Models.Sites.find({ parent: _id, status: 'active' }).then(editorial => res.json(editorial)).catch(next);
};
const childrens = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  const { _id } = req.site;
  const { _id: editorial_id } = req.editorial;

  Models.Sites.find({ parent: editorial_id, site: _id, status: 'active' }).then(editorial => res.json(editorial)).catch(next);
};


export const parameters = (appInstance) => {
  appInstance.param('site_id', (req, res, next, id) => {
    // Load Models
    const Models = req.app.getModels();

    Models.Sites.findOne({ _id: id }).then((site) => {
      if (!site) { return next(new Error('Site not found')); }
      req.site = site;
      return next();
    }).catch(next);
  });
  appInstance.param('editorial_id', (req, res, next, id) => {
    const Models = req.app.getModels();

    Models.Sites.findOne({ _id: id }).then((editorial) => {
      if (!editorial) { return next(new Error('Editorial not found')); }
      req.editorial = editorial;
      return next();
    }).catch(next);
  });
};

export const appRoutes = (appInstance) => {
  appInstance.get('/sites/:site_id/editorials/:editorial_id/childrens', bearerToken, childrens);
  appInstance.get('/sites/:site_id/editorials/:editorial_id', bearerToken, editorials);
  appInstance.get('/sites/:site_id/editorials', bearerToken, editorials);
  appInstance.get('/sites/:site_id', bearerToken, show);
  appInstance.put('/sites/:site_id', bearerToken, edit);
  appInstance.delete('/sites/:site_id', bearerToken, remove);
  appInstance.post('/sites', bearerToken, create);
  appInstance.get('/sites', bearerToken, index);
};

export default appRoutes;
