import _ from 'lodash';
import logger from 'debug';
import { bearerToken } from '../../middlewares/auth';

const debug = logger('Contents:');

const prepareValues = (req, onlyContent = false) => {
  let fields = ['title', 'slug', 'type', 'fullText', 'status'];
  if (!onlyContent) {
    fields = _.union(fields, ['thumbnail["id"]', 'files', 'editorial', 'tags']);
  }
  const values = _.pick.apply(this, _.union([req.body], fields));
  if (values.thumbnail) values.thumbnail = values.thumbnail.id;
  if (values.files) {
    values.files = _.map(values.files, n => n.id);
  }
  return values;
};
// INDEX Method
const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  const { _id } = req.site;
  // Query Configs
  const query = _.transform(
    _.assign({ site: _id },
    _.pick(req.query, 'title', 'status')),
    (result, value, key) => {
      const obj = {};
      obj[key] = (key === 'site') ? value : new RegExp(value, 'i');
      _.assign(result, obj);
    },
  {});
  debug('Query a ser executada:');
  debug(query);
  // Count Data
  return Models.Contents.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Contents.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// Show Method
const show = (req, res, next) => {
  req.content
  .populate('site')
  .populate('thumbnail')
  .populate('editorial')
  .populate('files')
  .populate('tags')
  .execPopulate()
  .then(() => {
    res.json(req.content);
  })
  .catch(next);
};
// Edit Method
const edit = (req, res, next) => {
  if (!req.content) return next(new Error('Content not found'));
  return res.json(req.content);
};
// Remove Method
const remove = (req, res, next) => {
  req.content.remove()
  .then(() => res.status(201).end())
  .catch(next);
};

// Create Method
const create = (req, res, next) => {
  const { Contents, Files, Tags } = req.app.getModels();
  const values = prepareValues(req, true);
  const { _id: siteId } = req.site;
  values.site = siteId;
  req.content = new Contents(values);
  req.content.save()
  .then((content) => {
    req.content = content;
  })
  .then(() => {
    const queries = [];
    if (_.pick(prepareValues(req, false), 'files')) {
      const filesVal = _.pick(prepareValues(req, false), 'files');
      const files = _.transform(filesVal.files, (result, value) => {
        const { _id: id } = value;
        result.push(id);
      }, []);
      queries.push(Files.find({ _id: { $in: files } }));
    }
    if (_.pick(prepareValues(req, false), 'tags')) {
      const tagsVal = _.pick(prepareValues(req, false), 'tags');
      const tags = _.transform(tagsVal.tags, (result, value) => {
        const { _id: id } = value;
        result.push(id);
      }, []);
      queries.push(Tags.find({ _id: { $in: tags } }));
    }
    if (req.body.editorial) {
      queries.push(Tags.find({ _id: { $in: [req.body.editorial] } }));
    }
    return Promise.all(queries);
  })
  .then((queries) => {
    _.map(queries, (promise) => {
      const item = promise[0];
      debug(item);
    });
  })
  .catch(err => next(err));
};

export const parameters = (appInstance) => {
  appInstance.param('content_id', (req, res, next, id) => {
    // Load Models
    const { _id: siteId } = req.site;
    const Models = req.app.getModels(siteId);
    const query = { _id: id };

    if (req.site) { query.site = siteId; }

    Models.Contents.findOne(query).then((content) => {
      if (!content) { return next(new Error('Content not found')); }
      req.content = content;
      return next();
    }).catch(next);
  });
};
export const appRoutes = (appInstance) => {
  appInstance.get('/sites/:site_id/contents/:content_id', bearerToken, show);
  appInstance.put('/sites/:site_id/contents/:content_id', bearerToken, edit);
  appInstance.delete('/sites/:site_id/contents/:content_id', bearerToken, remove);
  appInstance.post('/sites/:site_id/contents', bearerToken, create);

  appInstance.get('/sites/:site_id/contents', bearerToken, index);
};

export default appRoutes;
