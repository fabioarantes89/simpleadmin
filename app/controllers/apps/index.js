import _ from 'lodash';
import logger from 'debug';
import { bearerToken } from '../../middlewares/auth';

const debug = logger('Sites:');


// INDEX Method
const index = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();

  // Define Pagination Configs
  const page = (parseInt(req.query.page, 10)) ? (parseInt(req.query.page, 10) - 1) : 0;
  const perPage = (parseInt(req.query.perPage, 10)) ? parseInt(req.query.perPage, 10) : 10;
  const startItem = perPage * page;

  // Query Configs
  const query = _.reduce(
    _.assign({ status: 'active' },
    _.pick(req.query, 'name')), (result, value, key) => _.assign({}, result, _.setWith({}, key, new RegExp(value, 'i'))),
    {});

  debug(query);
  // Count Data
  return Models.Apps.count(query).then((count) => {
      // Format Response
    res.locals.count = count;
    res.locals.totalPages = Math.ceil(count / perPage);
    res.locals.currentPage = page + 1;
      // Find Itens
    return Models.Apps.find(query).limit(perPage).skip(startItem).exec();
  }).then((data) => {
    // Format Response
    res.locals.data = data;
    // Respond
    return res.json(res.locals);
  }).catch(next);
};
// CREATE Method
const create = (req, res, next) => {
  // Load Models
  const Models = req.app.getModels();
  // Filter Input values
  const values = _.pick(req.body, 'name', 'status');
  // Find Group
  req.appClient = new Models.Apps(values);
    // Save Site
  return req.appClient.save()
  .then(appClient => res.json(appClient))
  .catch(next);
};
const edit = (req, res, next) => {
  const values = _.pick(req.body, 'name', 'status');

  // Mutate Site
  _.assignIn(req.appClient, values);
  // Save User
  return req.appClient.save()
  .then(res.json)
  .catch(next);
};
const show = (req, res) => res.json(req.appClient);

// REMOVE Method
const remove = (req, res, next) => {
  req.appClient.remove().then(() => res.status(201).end()).catch(next);
};


export const parameters = (appInstance) => {
  appInstance.param('app_id', (req, res, next, id) => {
    // Load Models
    const Models = req.app.getModels();

    Models.Apps.findOne({ _id: id }).then((app) => {
      if (!app) { return next(new Error('App not found')); }
      req.appClient = app;
      return next();
    }).catch(next);
  });
};

export const appRoutes = (appInstance) => {
  appInstance.get('/apps/:app_id', bearerToken, show);
  appInstance.put('/apps/:app_id', bearerToken, edit);
  appInstance.delete('/apps/:app_id', bearerToken, remove);
  appInstance.post('/apps', bearerToken, create);
  appInstance.get('/apps', bearerToken, index);
};

export default appRoutes;
