import express from 'express';
import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';
import debug from 'debug';
import _ from 'lodash';
import bodyParser from 'body-parser';
import cors from 'cors';

// Passport Imports


// /Passport Imports
const debugg = debug('APP');

Promise.promisifyAll(fs);

export const app = express();


// CORS
app.use(cors({
  origin: 'http://localhost:4200',
  optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// parse application/json
app.use(bodyParser.json());




const loadControllersAndParameters = () => {
  app.getDatabase = () => app.locals.dataBase;
  app.getModels = () => app.locals.dataBase.getModels();
  debugg('Loading Controllers');
  return fs.readdirAsync(path.resolve(__dirname, 'controllers')).then((files) => {
    debugg('Controllers Found');
    debugg(files);
    return _.map(files, file => path.resolve(__dirname, 'controllers', file, 'index.js'));
  }).then(files => _.map(files, (file) => {
    debugg('Loading Controller');
    debugg(file);
    /* eslint-disable */
    const { parameters, appRoutes } = require(file);
    /* eslint-enable */
    if (parameters) parameters(app);
    appRoutes(app);
    return appRoutes;
  })).catch((err) => {
    debugg('Unable to load Routes');
    debugg(err);
  });
};
loadControllersAndParameters();
/* eslint-disable */
app.use(function (err, req, res, next){
  debugg('ERROR HAPPEND');
  if (err) {
    res.status(500).json(err);
  }
  return next();
});
/* eslint-enable */
export default app;
