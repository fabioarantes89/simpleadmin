export const bearerToken = (req, res, next) => {
  const { Tokens } = req.app.getModels();
  const { authorization } = req.headers;
  if (!authorization) return next(Error('Unauthorized Access'));

  const token = authorization.replace(/(Bearer |Bearer: )/i, '');
  return Tokens.findOne({ token }).populate('user app').exec().then((tk) => {
    if (!tk) return next(Error('Unauthorized Access'));
    req.auth = { user: tk.user, app: tk.app };
    return next();
  })
  .catch(err => next(err));
};

export default bearerToken;
