import process from 'process';

const env = (process.env.NODE_ENV) ? process.env.NODE_ENV : 'development';

export const configs = {
  production: {
    startupPort: 3000,
    database: 'mongodb://localhost/simpleadmin',
  },
  testing: {
    startupPort: 8080,
    database: 'mongodb://localhost/simpleadmin',
  },
  development: {
    startupPort: 8080,
    database: 'mongodb://localhost/simpleadmin',
  },
};

export const runtimeConfigs = configs[env];

export default runtimeConfigs;
