import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import moment from 'moment';

global.Promise = Promise;
const SALT_ROUNDS = 10;
const appDefSecret = 'ContainerDigital-Firebird-Mustang';


const init = (connection) => {
  const Name = 'Tokens';
  const Schemas = connection.Schema;
  const Schema = Schemas({
    token: {
      type: String,
    },
    user: {
      type: Schemas.Types.ObjectId,
      ref: 'Users',
    },
    app: {
      type: Schemas.Types.ObjectId,
      ref: 'Apps',
    },
    validUntil: {
      type: Date,
      default() {
        return moment().add(1, 'y').toDate();
      },
      required: true,
    },
  });
  Schema.pre('save', function SaveToken(next) {
    if (this.token && this.token.length >= 10) { return next(); }
    return bcrypt.genSalt(SALT_ROUNDS).bind(this).then(function SaveHash(salt) {
      return bcrypt.hash(`${this.user} - ${this.app}-${appDefSecret}`, salt).bind(this);
    })
    .then(function HashToken(hash) {
      this.token = hash;
      return next();
    })
    .catch(err => next(err));
  });
  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
