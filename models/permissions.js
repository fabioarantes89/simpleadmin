import uniqueValidator from 'mongoose-unique-validator';

const init = (connection) => {
  const Name = 'Permissions';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    module: {
      type: String,
      required: true,
    },
    action: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
      unique: true,
    },
  });
  Schema.plugin(uniqueValidator);

  Schema.pre('validate', (next) => {
    this.name = `${this.module}:${this.action}`;
    return next();
  });

  const Model = connection.model(Name, Schema);
  return {
    Schema, Model, Name,
  };
};

export default init;
