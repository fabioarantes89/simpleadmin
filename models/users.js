import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import _ from 'lodash';

global.Promise = Promise;
const SALT_ROUNDS = 10;


const init = (connection) => {
  const Name = 'Users';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      required: true,
    },
    group: {
      type: Schemas.Types.ObjectId,
      ref: 'Groups',
      required: true,
    },
  });

  Schema.method('comparePassword', function comparePassword(pass) {
    return bcrypt.compare(pass, this.password);
  });

  if (!Schema.options.toObject) Schema.options.toObject = {};
  Schema.options.toObject.transform = (doc, ret) => _.omit(ret, 'password');

  if (!Schema.options.toJSON) Schema.options.toJSON = {};
  Schema.options.toJSON.transform = (doc, ret) => _.omit(ret, 'password');

  Schema.pre('save', function SaveHash(next) {
    if (!this.isModified('password')) return next();
    return bcrypt.genSalt(SALT_ROUNDS)
    .then(salt => bcrypt.hash(this.password, salt))
    .then((hash) => {
      this.password = hash;
      return next();
    })
    .catch(err => next(err));
  });

  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
