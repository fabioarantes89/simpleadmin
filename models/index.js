import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';
import debug from 'debug';
import _ from 'lodash';
import mongoose from 'mongoose';
import { runtimeConfigs } from '../configs/environment';

const debugg = debug('MODELS');

mongoose.Promise = Promise;

mongoose.connect(runtimeConfigs.database);

Promise.promisifyAll(fs);

const Schemas = {};
const Models = {};

const loadModels = () => fs.readdirAsync(path.resolve(__dirname)).then(files => _.map(_.without(files, 'index.js'), file => path.resolve(__dirname, file))).then(files => _.map(files, (file) => {
  debugg('Loading Model');

  /* eslint-disable */
  const Module = require(file);
  /* eslint-enable */
  const { Schema, Model, Name } = Module.default(mongoose);

  Schemas[Name] = Schema;
  if (Model) {
    Models[Name] = Model;
  }


  return Model;
})).then(() => ({ getModels: () => Models, getSchemas: () => Schemas }));

export default loadModels;
