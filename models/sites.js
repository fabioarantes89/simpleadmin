const init = (connection) => {
  const Name = 'Sites';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    name: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    parent: {
      type: Schemas.Types.ObjectId,
      ref: 'Sites',
    },
    site: {
      type: Schemas.Types.ObjectId,
      ref: 'Sites',
    },
    uri: {
      type: String,
      required: true,
    },
    content_id: {
      type: Schemas.Types.ObjectId,
      ref: 'Contents',
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      required: true,
    },
  });
  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
