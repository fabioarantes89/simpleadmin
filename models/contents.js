const init = (connection) => {
  const Name = 'Contents';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    site: {
      type: Schemas.Types.ObjectId,
      ref: 'Sites',
    },
    editorial: {
      type: Schemas.Types.ObjectId,
      ref: 'Sites',
    },
    fullText: {
      type: String,
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      required: true,
    },
    thumbnail: {
      type: Schemas.Types.ObjectId,
      ref: 'Files',
    },
    files: [{
      type: Schemas.Types.ObjectId,
      ref: 'Files',
    }],
    tags: [{
      type: Schemas.Types.ObjectId,
      ref: 'Files',
    }],
  });

  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
