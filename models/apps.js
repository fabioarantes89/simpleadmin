import bcrypt from 'bcrypt';
import Promise from 'bluebird';
import uniqueValidator from 'mongoose-unique-validator';

global.Promise = Promise;
const SALT_ROUNDS = 10;


const init = (connection) => {
  const Name = 'Apps';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    name: {
      type: String,
      required: true,
    },
    client_id: {
      type: String,
      required: true,
      unique: true,
    },
    secret: {
      type: String,
      required: true,
      unique: true,
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      required: true,
    },
  });
  Schema.plugin(uniqueValidator);
  Schema.pre('validate', (next) => {
    if (!this.isModified('name')) return next();
    let genSalt = null;
    return bcrypt.genSalt(SALT_ROUNDS).then((salt) => {
      genSalt = salt;
      return bcrypt.hash(`${this.name}ContainerDigital${new Date()}`, salt);
    }).then((hash) => {
      this.client_id = hash;
      return bcrypt.hash(hash + (new Date()), genSalt);
    }).then((hash2) => {
      this.secret = hash2;
      return next();
    })
    .catch(err => next(err));
  });

  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
