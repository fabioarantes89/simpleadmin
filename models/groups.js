const init = (connection) => {
  const Name = 'Groups';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    name: {
      type: String,
      required: true,
    },
    permissions: [{
      type: Schemas.Types.ObjectId,
      ref: 'Permissions',
    }],
  });

  const Model = connection.model(Name, Schema);
  return {
    Schema, Model, Name,
  };
};

export default init;
