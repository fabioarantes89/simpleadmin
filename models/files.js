import moment from 'moment';

const init = (connection) => {
  const Name = 'Files';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    filename: {
      type: String,
      required: true,
    },
    fileType: {
      type: String,
      required: true,
      enum: ['URL', 'FILE'],
    },
    title: {
      type: String,
    },
    credits: {
      type: String,
    },
    description: {
      type: String,
    },
  });
  Schema.virtual('createdAt').get(function getCreatedAt() {
    const { _id } = this;
    return _id.getTimestamp();
  });
  Schema.virtual('path').get(function PathVirtual() {
    if (this.fileType && this.fileType === 'FILE') {
      return `/uploads/${moment(this.createdAt).format('YYYY/MM/DD')}/${this.filename}`;
    }
    return '';
  });
  Schema.set('toJSON', { getters: true, virtuals: true });

  const Model = connection.model(Name, Schema);
  return {
    Schema, Model, Name,
  };
};

export default init;
