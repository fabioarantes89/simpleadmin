const init = (connection) => {
  const Name = 'Tags';

  const Schemas = connection.Schema;

  const Schema = Schemas({
    name: {
      type: String,
      required: true,
    },
    taxonomy: {
      type: String,
      required: true,
    },
    thumbnail: {
      type: Schemas.Types.ObjectId,
      ref: 'Files',
    },
    infos: {
      type: String,
    },
    status: {
      type: String,
      enum: ['active', 'inactive'],
      required: true,
    },
    site: {
      type: Schemas.Types.ObjectId,
      ref: 'Sites',
      required: true,
    },
  });

  const Model = connection.model(Name, Schema);

  return {
    Schema, Model, Name,
  };
};

export default init;
