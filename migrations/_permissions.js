import _ from 'lodash';


const Permissions = (Models) => {
  const perms = _.flatten(_.concat(_.map(['groups', 'users', 'permissions'], module => _.map(['index', 'create', 'remove', 'edit'], method => ({ module, action: method, name: `${module}:${method}` })))));

  return Models.Permissions.insertMany(perms);
};

export default Permissions;
