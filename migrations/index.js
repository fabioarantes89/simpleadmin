import Promise from 'bluebird';
import path from 'path';
import fs from 'fs';
import debug from 'debug';
import _ from 'lodash';

const debugg = debug('Migrations');
Promise.promisifyAll(fs);


const loadMigrations = Models => fs.readdirAsync(path.resolve(__dirname)).then(files => _.map(_.filter(_.without(files, 'index.js'), file => !/^_/i.exec(file)), file => path.resolve(__dirname, file))).then(files => _.map(files, (file) => {
  debugg('Loading Migrations');
  const Migration = require(file);

  Migration.default(Models).then(() => {
    const fName = path.basename(file);
    const nFile = file.replace(fName, `_${fName}`);
    return fs.renameAsync(file, nFile);
  });

  return Migration;
})).then((migrations) => {
  debugg(migrations);
  return migrations;
});

export default loadMigrations;
