import logger from 'debug';
import _ from 'lodash';

const debug = logger('Migration');


const Migrate = (Models) => {
  let Group = null;
  return Models.Groups.findOne({ name: 'Administrators' }).exec().then((group) => {
    Group = group;
    return Models.Permissions.find({}).exec().then((permissions) => {
      const perms = _.reduce(permissions, (result, value) => {
        const { _id } = value;
        result.push(_id);
        return result;
      }, []);
      debug(perms);
      Group.permissions = perms;
      return Group.save();
    });
  });
};

export default Migrate;
